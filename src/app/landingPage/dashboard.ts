import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'dashboard',
    template: `
                <div class="bgr-blue-class">
                    <h2>{{welcomeMsg}}</h2>
                </div>    
                <nav>
                    <a routerLink="/list">list</a>
                    <a routerLink="/detail">detail</a>
                </nav>
            
                `
})
export class Dashboard implements OnInit {
    private welcomeMsg:String;
    constructor(private router:Router) { 
        this.welcomeMsg = 'yo yo ma';
    }
    ngOnInit() { }
}