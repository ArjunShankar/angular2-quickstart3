import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Dashboard } from './dashboard'
import { DataService } from '../common/services/data.service';

@Component({
    selector: 'my-app',
    providers:[DataService],
    template:`
<h1>topheader</h1>
    <router-outlet></router-outlet>
    `
})
export class AppComponent {
    constructor(){}
}