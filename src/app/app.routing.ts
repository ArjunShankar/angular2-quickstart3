import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPage } from './detailPage/detail.component';
import { ListPage } from './listPage/list.component';
import { Dashboard } from './landingPage/dashboard';

const appRoutes: Routes = [
  { path: 'dashboard', component: Dashboard },
  { path: 'detail', component: DetailPage },
  { path: 'detail/:id', component: DetailPage },
  { path: 'list', component: ListPage },
  { path: 'list2', redirectTo: '/list',pathMatch:'full' },
  { path: '', redirectTo: '/dashboard',pathMatch:'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
