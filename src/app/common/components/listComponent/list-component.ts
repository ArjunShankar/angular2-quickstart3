import { Component,Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'list-component',
    template:`
            <ul class="u_list">
                <li *ngFor="let item of dataProvider" (click)="onItemSelect(item)">
                    <span>{{item}}</span>
                </li>
            </ul>
            `
})
export class ListComponent implements OnInit {
    constructor(private router:Router) { }
    @Input() dataProvider:Array<any>;
   
    ngOnInit() {
        console.log("in list component");
    }
    onItemSelect(input:any){
        let link = ['/detail', input];
        this.router.navigate(link);
    }

}