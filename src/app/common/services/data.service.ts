import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { dummyNumbers } from'../dummyData/dummyData' 

@Injectable()
export class DataService {

    constructor(private http: Http) { }

    //dummy data
    getData():Promise<Number[]>{
        return Promise.resolve(dummyNumbers);
    }

    //dummy data - slowly
    getDataSlowly(): Promise<Number[]> {
        return new Promise<Number[]>(resolve =>
            setTimeout(resolve, 2000)) // delay 2 seconds
            .then(() => this.getData());
    }

    //api call - using promises
    getRemoteData(): Promise<Number[]> {
        return this.http.get('http://localhost:8081/intdata/get')
            .toPromise()
            .then(response => response.json().data as Number[])
            .catch(this.handleError);
    }

    handleError(err:any){
        console.log(err.json().msg);
        return Promise.reject(err.message || err);
    }

    getRemoteData2(): Observable<any[]> {
        return this.http.get('http://localhost:8081/objectdata/get')
            .map((r: Response) => r.json().data as any[])
            .catch(this.handleObservableError);
    }

    handleObservableError(error:any){
        console.error(error);
        return Observable.throw(error.json().error || 'Unknown server error');  // todo ... what after throwing error ?
    }

}