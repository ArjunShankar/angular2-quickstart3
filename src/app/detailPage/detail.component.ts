import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { DataService } from '../common/services/data.service';
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'detail-page',
    template: `
            <div>
                <span> This is the detail page </span>
                <div>
                    <h3> Adding to a List</h3>
                    <input #input1 (blur)="addToList(input1.value);input1.value=''">
                    <button (click)="addToList(input1.value)">Add</button>
                    <ol class="u_list">
                        <li *ngFor="let item of list2">
                            <span>{{item}}</span>
                        </li>
                    </ol>               
                </div>
                <div>
                <ol class="u_list">
                        <li *ngFor="let item of list3">
                            <span>{{item.label}}</span> &nbsp; <span>{{item.value}}</span>
                        </li>
                 </ol>
                
                
                </div>
            </div>    
            `
})
export class DetailPage implements OnInit {
    private list2:Array<string>;
    private list3:Array<string>;

    constructor(private route: ActivatedRoute,
                private dataService:DataService) {
        this.list2 = [];
        this.list3 = [];
     }

    addToList(input:string){
        if(input != '')
            this.list2.push(input);
    }

    ngOnInit(){
        console.log('in detail init',this.route.params);
        this.route.params.forEach((params: Params) => {
            console.log(params['id']);
        });

        this.dataService.getRemoteData2()
            .subscribe((receivedData: any) => {this.list3 = receivedData;})//TODO handle error here 
    }

    ngOnDestroy(){
        
    }
}