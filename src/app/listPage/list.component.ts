import { Component } from '@angular/core';
import { DataService } from '../common/services/data.service';


@Component({
    selector: 'list-page',
    template:`
             <h2> this is the list page </h2>
             <input [(ngModel)]="inputVar1" placeholder="enter text..."><span>{{inputVar1}}</span> 
             <div>
                <button (click)="getDataFromService()">GET DATA</button>
                <list-component [dataProvider]="list1"></list-component>
             </div>
             `
})
export class ListPage {
    private inputVar1:String;
    private list1:Array<any>;

    constructor(private dataService:DataService) {
         this.list1 = [];
     }

    onItemSelect(input:any){
        console.log('current selction :: '+input);
    }

    getDataFromService(){
      //this.list1 = this.dataService.getData();

      /*
        this.dataService.getDataSlowly()
            .then(result => this.list1 = result);

      */
        this.dataService.getRemoteData()
            .then(result => this.list1 = result);
    }

}