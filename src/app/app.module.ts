import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { routing } from './app.routing'
import { HttpModule }    from '@angular/http';

import { AppComponent } from './landingPage/app.component';
import { Dashboard } from './landingPage/dashboard';

import { ListPage } from './listPage/list.component'
import { DetailPage } from './detailPage/detail.component'
import { ListComponent } from './common/components/listComponent/list-component'

@NgModule({
    imports: [ BrowserModule,FormsModule,routing,HttpModule],
    declarations :[ AppComponent,Dashboard,ListComponent,ListPage,DetailPage ],
    bootstrap : [ AppComponent ]
})
export class AppModule { }